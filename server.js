require('dotenv').config()
var express = require('express')
var app = express()
var cors = require('cors')
var PORT = process.env.PORT || 3000
const bodyParser = require('body-parser')
app.use(bodyParser.json())
app.use(cors())
const config_server = process.env.DB_ATLAS_MONGO || process.env.DB_LOCAL_MONGO

var mongoose = require('mongoose')
mongoose.connect(config_server, {useNewUrlParser: true, useUnifiedTopology: true })

app.get('/', (req, res) => {
    res.send('this is marketplace')
})

// API
require('./app/routes/user.routes')(app)
require('./app/routes/product.routes')(app)
require('./app/routes/review.routes')(app)

app.listen(PORT, () => {
    console.log('Listening on port ' + PORT )
})