const mongoose = require('mongoose')

var reviewSchema = new mongoose.Schema({
    description: {
        type: String,
        required: true
    },
    product: { type: mongoose.Schema.Types.ObjectId, ref: 'Product' },
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' }

})

var Review = mongoose.model('Review', reviewSchema)

module.exports = Review