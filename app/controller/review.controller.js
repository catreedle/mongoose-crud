var Review = require('../models/review.model')
var Response = require('../middleware/Response')
var User = require('../models/user.model')
var Product = require('../models/product.model')

exports.reviewCreate = async (req, res) => {
    try {
        var review = await Review.create({
            description: req.body.description,
            user: req.userId,
            product: req.query.productid
        })
        await User.findByIdAndUpdate(review.user, { $push: { reviews: review._id } }, { useFindAndModify: false })
        await Product.findByIdAndUpdate(review.product, { $push: { reviews: review._id } }, { useFindAndModify: false })
        Response(res, true, "review created", review)
    } catch (error) {
        Response(res, false, "error to create review", error)
    }
}

exports.reviewShowAll = (req, res) => {
    Review.find({}).then(allreview => {
        Response(res, true, "all review is retrieved", allreview)
    }).catch(err => {
        Response(res, false, "can not retrieve review", err)
    })
}

exports.reviewShow = (req, res) => {
    Review.findById(req.params.id)
        .populate({
            path: 'user',
            select: 'username'
        })
        .populate({
            path: 'product',
            select: 'name'
        })
        .then(review => {
            if (review) {
                Response(res, true, "review is retrieved", review)
            } else {
                Response(res, false, "review not found", err)
            }

        })
        .catch(err => {
            Response(res, false, "catch error can not retrieve", err)
        })
}

exports.reviewUpdate = async (req, res) => {
    try {
        var review = await Review.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true})
        Response(res, true, "review is updated", review)
    } catch (err) {
        Response(res, false, "can not update", err)
    }
}

exports.reviewDelete = async (req, res) => {
    try {
        var review = await Review.findByIdAndDelete(req.params.id)
        await User.findByIdAndUpdate(review.user, { $pull: { reviews: review._id } })
        await Product.findByIdAndUpdate(review.product, { $pull: { reviews: review._id } })
        Response(res, true, "Review deleted", review)
    } catch (err) {
        Response(res, false, "error from handler", err)
    }
}