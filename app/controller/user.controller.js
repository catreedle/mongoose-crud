var User = require('../models/user.model')
var Product = require('../models/product.model')
var Review = require('../models/review.model')
var Response = require('../middleware/Response')
const bcrypt = require('bcrypt')
const saltRounds = 10
const jwt = require('jsonwebtoken')
const jwt_pass = "random string..."


exports.userCreate = (req, res) => {
    var newUser = new User({
        username: req.body.username,
        password: bcrypt.hashSync(req.body.password, saltRounds),
        email: req.body.email,
        image: req.body.image,
        reviews: [req.body.reviews],
        products: [req.body.products]
    })
    newUser.save().then((createdUser) => {
        Response(res, true, "user created", createdUser)
    }).catch((err) => {
        Response(res, false, "can not create user", err)
    })
}
// Response is a substitute to res.json()

exports.userShowAll = (req, res) => {
    User.find({}).then(alluser => {
        Response(res, true, "all user is retrieved", alluser)
    }).catch(err => {
        Response(res, false, "can't create user", err)
    })
}

exports.userShow = (req, res) => {
    User.findById(req.params.id)
        .populate({
            path: 'products',
            select: ['name', 'price', 'image']
        })
        .then(user => {
            if (user) {
                Response(res, true, "user retrieved", user)
            } else {
                Response(res, false, "can't get user", err)
            }

        })
        .catch(err => {
            Response(res, false, "error catch user retrieve", err)
        })
}

// exports.userDelete = (req, res) => {
//     User.findByIdAndRemove(req.params.id, { useFindAndModify: false })
//         .then(user => {
//             if (user) {
//                 Response(res, true, "user deleted", user)
//             } else {
//                 Response(res, false, "can't delete", err)
//             }
//         })
//         .catch(err => {
//             Response(res, false, "catch error delete", err)
//         })
// }

exports.userDelete = async (req, res) => {
    try {
        var user = await User.findByIdAndDelete(req.params.id)
        await Product.deleteMany({user: user})
        await Review.deleteMany({user: user})
        Response(res, true, "user deleted", user)
    } catch (err) {
        Response(res, false, "fail delete user", err)
    }
}

exports.userUpdate = (req, res) => {
    var updateUser = req.body
    if (updateUser.password) {
        updateUser.password = bcrypt.hashSync(req.body.password, saltRounds)
    }
    User.findByIdAndUpdate(req.params.id, {
        $set: updateUser
    }, {
        new: true,
        useFindAndModify: false
    }).then(updated => {
        Response(res, true, "user updated", updated)
    })
        .catch(err => {
            Response(res, false, "catch error update", err)
        })
}

exports.userLogin = (req, res) => {
    User.findOne({ username: req.body.username })
        .then(user => {
            if (!user) {
                res.send('user not found')
            } else {
                var hash = bcrypt.compareSync(req.body.password, user.password)
                if (hash) {
                    var token = jwt.sign({
                        username: user.username,
                        id: user._id
                    }, jwt_pass)

                    Response(res, true, "you're logged in", {token: token, id: user._id})
                } else {
                    Response(res, false, "wrong password")
                }
            }

        })
        .catch(err => {
            console.log('catch error')
            Response(res, false, "can not log in", err)
        })
}