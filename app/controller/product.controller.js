var Product = require('../models/product.model')
var User = require('../models/user.model')
var Response = require('../middleware/Response')

exports.productCreate = (req, res) => {
    Product.create({
        name: req.body.name,
        price: req.body.price,
        user: req.userId,
        image: req.body.image
    })
    .then(product => {
        User.findById(req.userId)
        .then(user => {
            user.products.push(product._id)
            user.products[0] == null ? user.products.splice(0, 1) : null
            user.save()
            Response(res, true, "product created", product)
        })
        .catch(errUser=>{
            Response(res,false,"something went wrong User", errUser)
        })
    })
    .catch(err => {
        Response(res,false,"something went wrong", err)
    })
            
}

exports.productShowAll = (req, res) => {
    Product.find({}).then(allproduct => {
        Response(res, true, "all product is retrieved", allproduct)
    }).catch(err => {
        Response(res, false, "can not retrieve product", err)
    })
}

exports.productShow = (req, res) => {
    Product.findById(req.params.id)
        .populate({
            path: 'user',
            select: 'username'
        })
        .populate({
            path: 'reviews',
            populate: {
                path: 'user',
                select: ['username']
            }
        })
        .then(product => {
            if (product) {
                Response(res, true, "product is retrieved", product)
            } else {
                Response(res, false, "can not get product", err)
            }

        })
        .catch(err => {
            Response(res, false, "can not retrieve", err)
        })
}

exports.productDelete = (req, res) => {
    Product.findByIdAndRemove(req.params.id, { useFindAndModify: false })
        .then(product => {
            User.findByIdAndUpdate(req.userId, {$pull: {products:{$in:product._id}}})
            .then(() => {
                Response(res, true, "product is deleted", product)
            }).catch((errUser) => {
                Response(res, false, "handler user error", errUser)
            })
        }).catch((err) => {
            Response(res, false, "handler error", err)
        })
}

exports.productUpdate = (req, res) => {
    Product.findByIdAndUpdate(req.params.id, {
        $set: req.body
    }, {
        new: true
    })
        .then(updated => {
            Response(res, true, "product is updated", updated)
        })
        .catch(err => {
            Response(res, false, "can not update", err)
        })
}