const User = require('../models/user.model')
const Product = require('../models/product.model')
const Review = require('../models/review.model')
const Response = require('./Response')

exports.user = (req, res, next) => {
    User.findById(req.userId)
    .then((user) => {
        if(String(req.userId) == String(user._id)) {
            next()
        } else {
            Response(res, false, "you're not authorized")
        }


    }).catch((err) => {
        Response(res, false, "User authorization error", err)
    })
}

exports.product = (req, res, next) => {
    Product.findById(req.params.id)
    .then((product) => {
        if(String(req.userId) == String(product.user)){
            next()
        } else {
            Response(res, false, "you're not authorized")
        }

    }).catch((err) => {
        Response(res, false, "Product authorization error", err)
    })
}

exports.review = (req, res, next) => {
    Review.findById(req.params.id)
    .then((review) => {
        if(String(req.userId) == String(review.user)){
            next()
        } else {
            Response(res, false, "you're not authorized")
        }
    }).catch((error) => {
        Response(res, false, "Review authorization error", error)
    })
}