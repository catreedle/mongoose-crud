var User = require('../models/user.model')
var Response = require('./Response')
var jwt = require('jsonwebtoken')
var jwt_pass = 'random string...'

module.exports = (req, res, next) => {
    if(req.headers.authorization){
        jwt.verify(req.headers.authorization, jwt_pass, (err, decoded) => {
            if(err){
                Response(res, false, "can not decode", err )
            } else {
                User.findById(decoded.id)
                .then(user => {
                    if(user){
                        req.userId = user._id
                        req.username = user.username
                        next()
                    } else {
                        Response(res, false, "user not found")
                    }
                }).catch(errUser => {
                    Response(res, false, "authentication error", errUser)
                    console.log(req.userId)
                })
            }
        })
    } else {
        Response(res, false, "token is required")
    }
}